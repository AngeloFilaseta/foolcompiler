package compiler;

import java.util.stream.IntStream;

import compiler.AST.*;
import compiler.lib.*;

public class TypeRels {

	// valuta se il tipo "a" e' <= al tipo "b", dove "a" e "b" sono tipi di base: IntTypeNode o BoolTypeNode
	public static boolean isSubtype(TypeNode a, TypeNode b) {
		return a.getClass().equals(b.getClass()) //same class case
				|| 
				((a instanceof BoolTypeNode) && (b instanceof IntTypeNode)) // bool is subtype of int
				|| 
				(a instanceof ArrowTypeNode && b instanceof ArrowTypeNode && // if a and b are bot ArrowType node...
				((ArrowTypeNode) a).parlist.size() == ((ArrowTypeNode) b).parlist.size() && // and they share the same # of par
				isSubtype(((ArrowTypeNode) a).ret,((ArrowTypeNode) b).ret)) && // ret-type of a is subtype of b ret-type (Covariance)
				IntStream.range(0, ((ArrowTypeNode) a).parlist.size())	// all the parameters of a are subtype of b (Contravariance)
					.allMatch(i -> isSubtype(((ArrowTypeNode) b).parlist.get(i), ((ArrowTypeNode) a).parlist.get(i)));
	}

}
